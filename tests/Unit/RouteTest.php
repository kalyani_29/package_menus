<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\TestingWithFaker;
use Tests\TestCase;

class RouteTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicTest()
    {

        $response3 = $this->get('/menus');//ok
        $response3->assertStatus(200);

        // $response4 = $this->get('addMenuMappingItem');//ok
        // $response4->assertStatus(200);

       //$response = $this->get('editMenuMapping');
      //  $response = $this->call('GET', 'editMenuMapping');
       // $response->assertStatus(500);
        //$this->assertEquals(200, $response->getStatusCode());

        // $response5 = $this->post('/menus/update');//ok
        // $response5->assertStatus(200);

        $response6 = $this->get('deleteMenuMappingAndRoles');//ok
        $response6->assertStatus(200);

        $response7 = $this->get('userAccessRole');//ok
        $response7->assertStatus(200);

        $response8 = $this->post('save-user-access-role');//ok
        $response8->assertStatus(200);

        $response9 = $this->get('usermenuAccessTypes');//ok
        $response9->assertStatus(200);

       $response10 = $this->post('updateMenuList', ['list'=>3]);//ok
       $response10->assertStatus(200);

        $response1 = $this->get('menu_master_report');//ok
        $response1->assertStatus(200);

        $response2 = $this->get('export_to_excel');//ok
        $response2->assertStatus(200);
    }


}
