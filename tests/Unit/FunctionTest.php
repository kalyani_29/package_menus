<?php

namespace Tests\Unit;

use App\Package\Menus\src\controllers\Base\MenusBaseController;
use App\Package\Menus\src\models\MenusMapping;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;

use Tests\TestCase;
class FunctionTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */

    public function test1()   //test is ok for getMenuById function
    {
        $actual2=array(
            "id" => 1,
            "menu_master_id" => 408,
            "menu_id" => 1,
            "parent_id" => 0,
            "reference_id" => 0,
            "short_code" => "Home",
            "sequence" => 0,
            "description" => "Home",
            "url" => "#",
            "class" => "abc",
            "font_icon_class" => "F1",
            "styles" => "italic",
            "created_by" => 0,
            "updated_by" => 0,
            "is_deleted" => 0,
            "is_completed" => 0,
            "created_at" => "2022-01-27 12:22:17",
            "updated_at" => "2022-01-27 12:22:39",
            "language_key" => "EN"
        );

        $menuid=408;
        $controller = new MenusBaseController();
        $response = $controller->getMenuById($menuid,$parentid="",$active=null);
      // dd($response[0]);
        $yummy = json_decode($response[0]);
       //dd( $yummy);
        $newData = (array) $yummy;
        //dd($newData);
        $this->assertEquals($newData ,$actual2);

    }

    public function test2() //test code is ok for update_menu_mapping
    {
        $menuid=6;
        $menu=408;
        $sequence=4;
        $controller = new MenusBaseController();
        $response = $controller->update_menu_mapping($menuid, $menu, $sequence, $parentid = 0);
       $this->assertDatabaseHas('menu_mapping', $response=array(
                                                                'menu_id'=>$menuid,
                                                                'menu_master_id'=>$menu,
                                                                'sequence'=>$sequence)
                               );
    // $this->assertDatabaseHas('menu_mapping', $response=array('menu_id'=>6,
    //    'menu_master_id'=>408,
    //    'sequence'=>1) );//ok

    }

    public function test3()  //the test is ok for save_user_role_access
    {
        $role_data= array(
            'company_id' => 10,
            'role_id' => 102,
            'object_id' => 1,
            'object_item_id'=>0,
            'can_view' => 1,
            'can_add' => 1,
            'can_edit' =>1,
            'created_at' => "2022-01-31 10:57:46",
            'updated_at'=> "2022-01-31 10:57:46",
        );
        $company_id=10;
        $role=102;
        $menus=array(
        0 => "1",
        1 => "3",
        2 => "2",
        3 => "4",
        4 => "5",
        5 => "7"
        );
        $controller = new MenusBaseController();
        $response=$controller->save_user_role_access($role_data,$role,$company_id,$menus);
       //dd(  $response);
        $response= $this->assertTrue($response==true);
    }

    public function test4() //test is ok for getcompany_byrole
    {
        $role=101;
        $controller=new MenusBaseController();
        $response=$controller->get_company_byrole($role);
       //dd($response);
       $this->assertTrue($response==true);
    }

    public function test5() //test is ok for getMenuList()
    {
        $controller=new MenusBaseController();
        $response=$controller->getMenuList();
       //$this->assertTrue($response==true); //ok
         $this->assertEmpty($response); //ok
    }
}
